import pycelonis
import pm4py
from pycelonis.pql import PQL, PQLColumn, PQLFilter, OrderByColumn
import pandas as pd

# create a get_celonis object
celonis = pycelonis.get_celonis(base_url = 'academic-celonis-x509kq.eu-2.celonis.cloud',
                                api_token = 'NTg1YjI0YzUtNjI5Yi00NzRmLWE1ZmQtMWQxYzM3OGRkYWQ0OldNMk5ERkdCUUZER1FJeXdYaTY3dFprOCs5dExOd3M0czRtYXk0dmJyTFhJ',
                                key_type = 'USER_KEY')

# load the dataset
original_data = pm4py.read_xes('receipt.xes')

# in our academic license, we can only have at most 100,000 enteries
if len(original_data) < 100000:
   data = original_data.copy() 
else:
   # if there are more than 100,000 rows, select the first 100,000
   data = original_data[:100000].copy()

# create data pool, or get data pool if it already exists
try:
    data_pool = celonis.data_integration.get_data_pools().find("data_pool")
except:
    data_pool = celonis.data_integration.create_data_pool("data_pool")  

# create data model, or get data model if it already exists
try:
    data_model = data_pool.get_data_models().find("data_model")
except:
    data_model = data_pool.create_data_model("data_model")

# create table in data pool, or do nothing if table in data pool already exists
try:
    data_pool.create_table(df = data, table_name = "data_table",  drop_if_exists = False)
except:
    data_pool.get_tables()

# add the table from the pool to the model, or do nothing if it already exists
try:
    tables = data_model.add_table(name = "data_table", alias = "data_table")
except:

data_model.reload()

# process Configuration
tables = data_model.get_tables()
activities = tables.find("data_table")
process_configuration = data_model.create_process_configuration(activity_table_id = activities.id,
                                                                case_id_column = "case:concept:name",
                                                                activity_column = "concept:name",
                                                                timestamp_column = "time:timestamp",)
