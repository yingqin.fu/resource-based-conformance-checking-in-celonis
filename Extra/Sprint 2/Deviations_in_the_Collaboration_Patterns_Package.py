
# Deviations in the collaboration patterns: based on metrics 

data_pool = celonis.data_integration.get_data_pools().find("data_pool")
data_model = data_pool.get_data_models().find("data_model")

# create a PQL object
pql = PQL()

# extract these columns
pql.columns.append(PQLColumn(name="case:concept:name", query="\"data_table\".\"CASE:CONCEPT:NAME\""))
pql.columns.append(PQLColumn(name="concept:name", query='"data_table"."concept:name"'))
pql.columns.append(PQLColumn(name="org:resource", query='"data_table"."org:resource"'))
pql.columns.append(PQLColumn(name="time:timestamp", query='"data_table"."time:timestamp"'))

# load the columns to a dataframe
dataframe_metrics = data_model.export_data_frame(pql)

def collab_pattern_sna_generate(data, collaboration_patterns, resource_key = 'org:resource',
                                timestamp_key = 'time:timestamp', case_id_key = 'case:concept:name'):
    
    # Create sna for handover of work,subcontracting or working together.
    if collaboration_patterns == "handover of work":
        sna = pm4py.discover_handover_of_work_network(data, resource_key = resource_key, 
                                                      timestamp_key = timestamp_key, case_id_key = case_id_key)
    elif collaboration_patterns == "subcontracting":
        sna = pm4py.discover_subcontracting_network(data, resource_key = resource_key, 
                                                    timestamp_key = timestamp_key, case_id_key = case_id_key)
    elif collaboration_patterns == "working together":
        sna = pm4py.discover_working_together_network(data, resource_key = resource_key,
                                                      timestamp_key = timestamp_key, case_id_key = case_id_key)
    else:
        # Invalid value warning
        print("Invalid data or collaboration patterns. please provide valid data or collaboration patterns.")
        
    return sna

def collab_pattern_resource_ranking(data, collaboration_patterns, resource_key='org:resource',
                                    timestamp_key = 'time:timestamp', case_id_key = 'case:concept:name'):
    # Get sna
    sna = collab_pattern_sna_generate(data, collaboration_patterns, resource_key = resource_key,
                                      timestamp_key = timestamp_key, case_id_key = case_id_key)
    
    # Convert to dataframe
    to_df = pd.DataFrame.from_dict(sna.connections, orient = 'index', columns = ['Value'])

    # Sort by value in descending order
    resource_ranking = to_df.sort_values(by = 'Value', ascending = False)
    
    return resource_ranking

def collab_pattern_resource_matrix(data, collaboration_patterns, resource_key = 'org:resource',
                                   timestamp_key = 'time:timestamp', case_id_key = 'case:concept:name'):

    sna = collab_pattern_sna_generate(data, collaboration_patterns, resource_key = resource_key,
                                      timestamp_key = timestamp_key, case_id_key = case_id_key)

    # get resources name
    resources = set()
    for key in sna.connections.keys():
        resources.add(key[0])
        resources.add(key[1])
    resource_names = list(resources)

    # creat DataFrame
    resource_matrix = pd.DataFrame(index=resource_names, columns=resource_names)

    # Input Data to Dataframe
    for key, value in sna.connections.items():
        resource_matrix.loc[key[0], key[1]] = value

    return resource_matrix

# Select collaboration pattern handover of work, subcontracting or working together
collaboration_patterns = "working together"

# Ranking between resources

collab_pattern_resource_df = collab_pattern_resource_ranking(dataframe_metrics, collaboration_patterns)
collab_pattern_resource_df.reset_index()

# Matrix between resources
collab_pattern_resource_matrix = collab_pattern_resource_matrix(dataframe_metrics, collaboration_patterns)
collab_pattern_resource_matrix.reset_index()

# use the collab_pattern_sna_generate to visualize the sna
sna = collab_pattern_sna_generate(dataframe_metrics, collaboration_patterns)
