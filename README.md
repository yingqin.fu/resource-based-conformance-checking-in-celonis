# Resource-Based Conformance Checking in Celonis



## Sprint 1

We had to make an educational account in Celonis. From that we got our key and used PyCelonis to connect the data with Celonis. Due to our account being just educational, we could not work on event logs with more than 100,000 records.


### Sub-task identification and assignment


For the Sprint 1, we identified 3 sub-tasks on which each one of us will work on. The tasks are:

* Identification of batches by Yashvardhan Singh Rautela
* Activities repeated by different resources by Yingqin Fu
* Resource activity performance by Yixiao Cai


### Running the tests

We used receipt.xes event log for our implementation.
We have also tested the code on interval_event_log.xes and BPI_Challenge_2012.xes event logs.

More information can be found in the sprint document.


## Sprint 2



### Sub-task identification and assignment


For the Sprint 2, we identified 3 sub-tasks on which each one of us will work on. The tasks are:

* Deviations in the work pattern and segregation of duties by Yashvardhan Singh Rautela and Yingqin Fu
* Deviations in the collaboration patterns by Yixiao Cai



### Running the tests

We used receipt.xes event log for our implementation. We have created 4 test classes to test the 4 generated dataframes during the analysis process, namely TestDataFrame,TestFourEyeDataFrame,TestRolesDataframe, TestActivityDifferentResources, and TestCollabPatternResource. We used the unit test to perform a basic test of the resulting dataframe, including the size and name of the dataframe, and its consistency with the final result.


More information can be found in the sprint document.


## Sprint 3



### Sub-task identification and assignment


For the Sprint 3, we identified 2 sub-tasks on which each one of us will work on. The tasks are:

* GUI Implementation by by Yashvardhan Singh Rautela
* Packaging and Testing by Yixiao and Yingqin.

More information can be found in the sprint document.
